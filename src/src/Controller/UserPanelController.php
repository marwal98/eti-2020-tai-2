<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditUserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class UserPanelController extends AbstractController
{

    /**
     * @Route("/login/edit/{id}", name="edit_user")
     * Method({"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return Response
     */

    public function editUser(Request $request, $id)
    {

       $entityManager = $this->getDoctrine()->getManager();
       $user =  $entityManager->getRepository(User::class)->find($id);

       $form = $this->createForm(EditUserType::class, $user);

       $form ->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid())
       {

           $data = $form->getData();

           $entityManager->persist($data);
           $entityManager->flush();

           $this->addFlash('notice','Your changes were saved!');


       }

        return $this->render('security/edit_user.html.twig', array(
            'form'=> $form->createView()
        ));


    }

}