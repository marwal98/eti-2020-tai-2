<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Comments;
use App\Entity\User;
use App\Form\AddCommentType;
use App\Form\AddPostType;
use App\Form\EditPostType;
use App\Repository\BlogPostRepository;
use App\Repository\CommentsRepository;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class EtiController
 */
class EtiController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function blogHomepage()
    {
        return $this->render('eti/blog/homepage.html.twig');
    }

    /**
     * @Route("/first/page", name="first_page")
     * @param TranslatorInterface $translator
     * @return Response
     * @throws Exception
     */
    public function randomNumber(TranslatorInterface $translator)
    {
        $number = random_int(0, 100);

        return $this->render('eti/blog/first_page.html.twig', [
            'number' => $number,
            'translated_php' => $translator->trans('Translated string'),
            'translated_php_pl' => $translator->trans('Translated string', [], 'messages', 'pl_PL')
        ]);
    }


    /**
     * @Route("posts/list", name="post_listing")
     *
     * @return Response
     * @throws Exception
     */
    public function listBlogPosts()
    {
        $repository = $this->getDoctrine()->getRepository(BlogPost::class);
        $articles = $repository->findBy([
            'is_visible'=>'1'
        ]);

        return $this->render('eti/blog/posts.html.twig', [
            'articles' => $articles
        ]);
    }


    /**
     * @Route("posts/view/{id}", name="post_details", requirements={"id":"\d+"})
     * @param $id
     * @param Request $request
     * @param User $user
     * @param BlogPost $post
     * @param CommentsRepository $commentsRepository
     * @return Response
     */
    public function postDetails($id, Request $request, User $user, BlogPost $post, CommentsRepository $commentsRepository)
    {
        $article = $this->getDoctrine()->getRepository(BlogPost::class)->find($id);

        if(!$article)
        {
            throw $this->createNotFoundException('No article found for id'.$id);
        }

        $newComment = new Comments();
        $comments = $commentsRepository->findBy(['post' => $post]);
        $form = $this->createForm(AddCommentType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $content = $form->getData()->getContent();

            if ($this->getUser() == true)
            {
                $userName = $this->getUser()->getUsername();


            }
            else {
                $userName = null;
                $idUser = null;
            }


            $newComment->setUsername($userName);
            $newComment->setContent($content);
            $newComment->setCreationDate(new \DateTime());
            $newComment->setUser($user);
            $newComment->setPost($post);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newComment);
            $entityManager->flush();




            $this->addFlash('notice','Your comment has been added!');

        }



        return $this->render('eti/blog/post_view.html.twig', [
            'articles' => $article,
            'form' => $form->createView(),
            'comments' => $comments

        ]);
    }

    /**
    * @Route("secret/area", name="secret_area")
     */

    public function secretArea()
    {
        $post = $this->getDoctrine()->getRepository(BlogPost::class)->findBy([
            'logged_users_only' => '1'
        ]);

        return $this->render('eti/blog/secret_area.html.twig', [
          'posts' => $post
        ]);
    }

    /**
     * @Route("posts/add", name="add_post")
     * @param Request $request
     * @return Response
     */

    public function addPost(Request $request)
    {
        $post = new BlogPost();
        $id = $this->getUser()->getId();

        $form = $this->createForm(AddPostType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $title = $form->getData()->getTitle();
            $summary = $form->getData()->getSummary();
            $content = $form->getData()->getContent();
            $isVisible = $form->getData()->getIsVisible();
            $forLogged = $form->getData()->getLoggedUsersOnly();
            $allowComment = $form->getData()->getAllowComment();
            $allowCommentAnonymous= $form->getData()->getAllowCommentAnonymous();

            $post->setTitle($title);
            $post->setSummary($summary);
            $post->setContent($content);
            $post->setCreationDate(new \DateTime());
            $post->setCreatorId($id);
            $post->setIsVisible($isVisible);
            $post->setLoggedUsersOnly($forLogged);
            $post->setAllowComment($allowComment);
            $post->setAllowCommentAnonymous($allowCommentAnonymous);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash('notice','Your post was added!');
        }

        return $this->render('eti/blog/add_post.html.twig', [
           'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("posts/edit/{id}", name="edit_post")
     * @param BlogPost $blogPost
     * @param Request $request
     * @return Response
     */


    public function editPost(BlogPost $blogPost, Request $request, $id)
    {
        $user_id = $this->getUser()->getId();
        $creator_id = $blogPost->getCreatorId();

        if ($user_id != $creator_id)
        {
            $this->render('errors/401.html.twig');
        }



        $entityManager = $this->getDoctrine()->getManager();
        $postManager = $entityManager->getRepository(BlogPost::class)->find($id);

        $form = $this->createForm(EditPostType::class, $postManager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $title = $form->getData()->getTitle();
            $summary = $form->getData()->getSummary();
            $content = $form->getData()->getContent();
            $isVisible = $form->getData()->getIsVisible();


            $postManager->setTitle($title);
            $postManager->setSummary($summary);
            $postManager->setContent($content);
            $postManager->setIsVisible($isVisible);


            $entityManager->persist($postManager);
            $entityManager->flush();

            $this->addFlash('notice','Your changes were saved!');
        }

        return $this->render('eti/blog/edit_post.html.twig',[
           'form' => $form->createView(),
        ]);

    }
}